package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task findByName(String name) {
        for (Task task : list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String name, final Status status) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        final List<Task> tasksByProject = new ArrayList<>();
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()))
                tasksByProject.add(task);
        }
        return tasksByProject;
    }

    @Override
    public List<Task> removeAllTaskByProjectId(final String projectId) {
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()))
                task.setProjectId(null);
        }
        return list;
    }

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        final Task task = findById(taskId);
        if (task == null)
            return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String projectId, final String taskId) {
        final Task task = findById(taskId);
        if (task == null)
            return null;
        task.setProjectId(null);
        return task;
    }

}
