package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    public E add(E entity) {
        list.add(entity);
        return entity;
    }

    public void remove(E entity) {
        list.remove(entity);
    }

    public List<E> findAll() {
        return list;
    }

    public List<E> findAll(Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(this.list);
        entities.sort(comparator);
        return entities;
    }

    public void clear() {
        list.clear();
    }

    public E findById(final String id) {
        for (E entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    public E findByIndex(final Integer index) {
        return list.get(index);
    }

    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }


    public E removeByIndex(final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    public boolean existById(final String id) {
        final E entity = findById(id);
        return entity != null;
    }

    public boolean existByIndex(final int index) {
        if (index < 0) return false;
        return index < list.size();
    }

    public Integer getSize() {
        return list.size();
    }

}
