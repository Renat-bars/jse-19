package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.IProjectRepository;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(final String name) {
        for (Project project : list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project startById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(String id) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeProjectStatusById(String id, Status status) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(Integer index, Status status) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(String name, Status status) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}
