package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task findByName(String name);

    Task removeByName(String name);

    Task startById(String id);

    Task startByIndex(Integer id);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusByName(String name, Status status);

    List<Task> findAllTaskByProjectId(String projectId);

    List<Task> removeAllTaskByProjectId(String projectId);

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String projectId, String taskId);

}
