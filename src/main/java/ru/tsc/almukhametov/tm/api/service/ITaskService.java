package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.api.IService;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Task;

public interface ITaskService extends IService<Task> {

    void create(String name, String description);

    Task findByName(String name);

    Task removeByName(String name);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    Task startById(String id);

    Task startByIndex(Integer id);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusByName(String name, Status status);

}
