package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.api.IService;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;

public interface IProjectService extends IService<Project> {

    void create(String name, String description);

    Project findByName(String name);

    Project removeByName(String name);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusByName(String name, Status status);

    Project setStatus(Status status);

}
