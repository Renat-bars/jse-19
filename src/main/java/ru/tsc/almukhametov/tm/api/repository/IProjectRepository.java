package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project findByName(String name);

    Project removeByName(String name);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusByName(String name, Status status);

}
